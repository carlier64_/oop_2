﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop2
{

    class c_vector2
    {
        private int x, y;

        public int m_ix
        {
            get
            {
                return x;
            }
            set
            {
                if (value > 0 && value < Console.WindowWidth)
                {
                    x = value;
                }
                else
                {
                    exception("Rossz érték");
                }
            }
        }

        public int m_iy
        {
            get
            {
                return y;
            }
            set
            {
                if (value > 0 && value < Console.WindowHeight)
                {
                    y = value;
                }
                else
                {
                    exception("Rossz érték");
                }
            }
        }

        private void exception(string str_exception)
        {
            throw new FormatException(str_exception);
        }

        public c_vector2(int fx, int fy)
        {
            this.x = fx;
            this.y = fy;
        }

        public c_vector2()
        {
            this.x = this.y = 0;      
        }

        public void print()
        {
            Console.SetCursorPosition(m_ix, m_iy);
            Console.WriteLine("x");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {

            c_vector2 vec = new c_vector2();
            vec.m_ix = -10;
            vec.m_iy = -5;

            vec.print();
            Console.ReadKey();

        }
    }
}
